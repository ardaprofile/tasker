# taskManager

Task Manager Project 
## A Fullstack Application to manage tasks 

Technologies Used:
- Node js,
- PostgreSQL,
- pg-promise library,
- EJS templating Engine,
- Express
- JsonWebToken(Jwt)
- bcrypt

 ![Home](public/assets/tasker1.PNG)
 ![Add task](public/assets/tasker2.PNG)
 ![Register](public/assets/tasker3.PNG)
 ![Login](public/assets/tasker4.PNG)
 ![Forgot Password](public/assets/tasker5.PNG)

