require('dotenv').config();
const pgp = require('pg-promise')()

db_user = process.env.DB_USER
db_password = process.env.DB_PASSWORD
db_host = process.env.DB_HOST
db_port = process.env.DB_PORT
db_database = process.env.DB_DATABASE
let connection;

if (process.env.SSL) {
  connection = {
    connectionString: `postgres://${db_user}:${db_password}@${db_host}:${db_port}/${db_database}`,
    ssl: { rejectUnauthorized: false },
  }
} else {
  
  connection = `postgres://${db_user}:${db_password}@${db_host}:${db_port}/${db_database}`
}
// console.log(connection);

db = pgp(connection)

module.exports = db