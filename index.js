const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
dotenv.config();
const session = require('express-session');
const ejs = require('ejs');
const expressLayouts = require('express-ejs-layouts')
const methodOverride = require('method-override');
const middleware = require('./middlewares')

const PORT = process.env.PORT || 8005;

// getting user routes

const loginRoute = require('./routes/login.js');
const logoutRoute = require('./routes/logout.js');
const registerRoute = require('./routes/register.js');
const passwordRoutes = require('./routes/password');
const confirmRoute = require('./routes/confirmreg');
const userRoutes = require('./routes/users');

app.use(methodOverride('_method'));

// function for calculating the remaining days
app.locals.remainingDays = (date) => {
 const todaysDate = new Date()
 //console.log(todaysDate);
 const dataValue = new Date(date)
 //  console.log(dataValue);
 const daysRemaining = dataValue.getTime() - todaysDate.getTime()
 const mins = Math.round(daysRemaining / 1000 / 60)
 const hours = Math.round(mins / 60)
 let days = Math.round(hours / 24) + 1;
 // console.log(days);
 days = days == 1 ? days + ' day' : days + ' days'
 return days;
}

app.use('/static', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressLayouts)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


const maxAge = 1000 * 24 * 60 * 60;
app.use(
  session({
    secret: 'secretkey',
    saveUninitialized: true,
    resave: false,
   cookie :{ maxAge : maxAge}
  })
)



app.use('/login', loginRoute);
app.use('/logout', logoutRoute);
app.use('/user', confirmRoute);
app.use('/register', registerRoute);
app.use('/password', passwordRoutes);
app.use('/', middleware.logOutCheck, userRoutes)



app.listen(PORT, () => {
 console.log(`Server is started on ${PORT}`);
});


