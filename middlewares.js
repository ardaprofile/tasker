const jwt = require('jsonwebtoken');


// middleware for logout check
const logOutCheck = (req, res, next) => {
  if (!req.session.authtoken) {
    res.clearCookie('connect.sid');
    return res.redirect('/login')
  } else {
    next();
  }
}

// middleware for login check
const logInCheck = (req, res, next) => {
  // check json web token exists & is verified
  let token = req.session.authtoken;
  if (token) {
    console.log('inside token present')
   jwt.verify(token, 'secretkey', (err, decodedToken) => {
     if (err) {
       console.log(err.message)
       console.log("token is expired");
       next();
     } else {
       console.log(decodedToken);
       
       res.redirect('/');
     }
   })
  } else {
    next();
  }
}

module.exports = { logInCheck,logOutCheck}
