const editform = document.getElementById('editform')

const taskName = document.getElementById('editTaskName')
const assignedTo = document.getElementById('editAssignedTo')
const duedate = document.getElementById('date_picker')
const status = document.getElementById('status')
const description = document.getElementById('editTaskDescription')

const tasknameEmptyError = document.getElementById('tasknameEmptyError')
const assignedToEmptyError = document.getElementById('assignedToEmptyError')
const dueDateEmptyError = document.getElementById('dueDateEmptyError')
const statusEmptyError = document.getElementById('statusEmptyError')
const descriptionEmptyError = document.getElementById('descriptionEmptyError')

let isFormValid

editform.addEventListener('submit', (e) => {
  clearValidation()

  // validating taskname - checking its empty or not
  if (!checkEmpty(taskName)) {
    setError(tasknameEmptyError)
  }

  // validating assignedTo is empty or not
  if (!checkEmpty(assignedTo)) {
    setError(assignedToEmptyError)
  }

  // validating duedate  is empty or not
  if (!checkEmpty(duedate)) {
    setError(dueDateEmptyError)
  }

  // validating status is empty or not
  if (!checkEmpty(status)) {
    setError(statusEmptyError)
  }

  // validating description is empty or not
  if (!checkEmpty(description)) {
    setError(descriptionEmptyError)
  }

  if (!isFormValid) {
    e.preventDefault()
  }




});


// function for checking a value is empty or not 
const checkEmpty = (input) => input.value.trim() !== '';

// function for setting error - with classlist
const setError = (inputError) => {
  inputError.classList.remove('hidden');
  inputError.classList.add('display');
  isFormValid = false;
}



const clearValidation = () => {
 isFormValid = true;

 tasknameEmptyError.classList.remove('display');
 tasknameEmptyError.classList.add('hidden');

 assignedToEmptyError.classList.remove('display');
 assignedToEmptyError.classList.add('hidden');

 dueDateEmptyError.classList.remove('display');
 dueDateEmptyError.classList.add('hidden');

statusEmptyError.classList.remove('display');
statusEmptyError.classList.add('hidden');

descriptionEmptyError.classList.remove('display')
descriptionEmptyError.classList.add('hidden')

}


