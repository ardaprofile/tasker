// this page is responsible for doing the front-end validation of the login page.

const form = document.getElementById('form');

const email = document.getElementById('email');


const emailEmptyError = document.getElementById('emailEmptyError')
const emailInvalidError = document.getElementById('emailInvalidError')
const emailNotExistError = document.getElementById('emailNotExistError')
const emailNotConfirmedError = document.getElementById('emailNotConfirmedError')


let isFormValid;

form.addEventListener('submit', (e) => {
  clearValidation()

  // validating email is empty or not and is not invalid format
  if (!checkEmpty(email)) {
    setError(emailEmptyError)
  } else if (!emailValid(email.value)) {
    setError(emailInvalidError)
 }
 
 
 if (!isFormValid) {
    e.preventDefault()
  }


});

// function for checking the input value empty or not
const checkEmpty = (input) => input.value !== '';

// function for checking the email  valid with regex
const emailValid = (inputValue) => {
  const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
  return re.test(inputValue);
}

// function for setting error - with classlist
const setError = (inputError) => {
  inputError.classList.remove('hidden');
  inputError.classList.add('display');
  isFormValid = false;
}

const clearValidation = () => {
 isFormValid = true;

  emailEmptyError.classList.remove('display')
  emailEmptyError.classList.add('hidden')

  emailInvalidError.classList.remove('display')
  emailInvalidError.classList.add('hidden')
 
  emailNotExistError.classList.remove('display')
  emailNotExistError.classList.add('hidden')

 
  emailNotConfirmedError.classList.remove('display')
  emailNotConfirmedError.classList.add('hidden')
}
