// this page is responsible for doing the front-end validation of the reset password page.

const resetform = document.getElementById('resetform');


const passwordEmptyError = document.getElementById('passwordEmptyError')
const passwordInvalidError = document.getElementById('passwordInvalidError')
const confirmPasswordEmptyError = document.getElementById('confirmPasswordEmptyError')
const passwordNotMatchingError = document.getElementById('passwordNotMatchingError')

resetform.addEventListener('submit', (e) => {
  clearValidation()

  // validating password checking password is empty or not and is not invalid format
  if (!checkEmpty(password)) {
    setError(passwordEmptyError)
  } else if (!passwordValid(password.value)) {
    setError(passwordInvalidError)
  }

  // validating password checking password is empty or not and is not invalid format
  if (!checkEmpty(confirm_password)) {
    setError(confirmPasswordEmptyError)
  } else if (!passwordMatch(password.value, confirm_password.value)) {
    setError(passwordNotMatchingError)
  }

  if (!isFormValid) {
    e.preventDefault()
  }
})

// function for checking the input value empty or not
const checkEmpty = (input) => input.value !== '';


// function for checking the password valid with regex
const passwordValid = (inputValue) => {
  const re = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;
  return re.test(inputValue);
}

// function for checking the password is matching with the confirm_password
const passwordMatch = (password, confirm_password) => password === confirm_password;


// function for setting error - with classlist
const setError = (inputError) => {
  inputError.classList.remove('hidden');
  inputError.classList.add('display');
  isFormValid = false;
}


const clearValidation = () => {
 isFormValid = true;
 
 passwordEmptyError.classList.remove('display');
 passwordEmptyError.classList.add('hidden');


 passwordInvalidError.classList.remove('display');
 passwordInvalidError.classList.add('hidden');

 confirmPasswordEmptyError.classList.remove('display');
 confirmPasswordEmptyError.classList.add('hidden');

 passwordNotMatchingError.classList.remove('display');
 passwordNotMatchingError.classList.add('hidden');

}
