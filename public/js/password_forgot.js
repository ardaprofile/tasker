const validation = ( email, emailEmptyError, emailInvalidError, setError) => {
  // function for checking the input value empty or not
  const checkEmpty = (inputValue) => inputValue !== ''

  // function for checking the email  valid with regex
  const emailValid = (inputValue) => {
    const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
    return re.test(inputValue)
  }

  // validating email - checking email is empty or not and not invalid format
  if (!checkEmpty(email)) {
    setError(emailEmptyError)
  } else if (!emailValid(email)) {
    setError(emailInvalidError)
  }
}

module.exports = validation;