// this page is responsible for doing the front-end validation of the registration page.

const form = document.getElementById('authform');
const name = document.getElementById('name');
const email = document.getElementById('email');
const password = document.getElementById('password');
const confirm_password = document.getElementById('confirm_password');


const nameEmptyError = document.getElementById('nameEmptyError');
const emailEmptyError = document.getElementById('emailEmptyError');
const emailInvalidError = document.getElementById('emailInvalidError');
const emailExistError = document.getElementById('emailExistError');
const passwordEmptyError = document.getElementById('passwordEmptyError');
const passwordInvalidError = document.getElementById('passwordInvalidError');
const confirmPasswordEmptyError = document.getElementById('confirmPasswordEmptyError');
const passwordNotMatchingError = document.getElementById('passwordNotMatchingError');



let isFormValid;

form.addEventListener('submit', (e) => {
  
  clearValidation();

  // validating name - checking its empty or not
  if (!checkEmpty(name)) {
    setError(nameEmptyError);
  }

  // validating email is empty or not and is not invalid format
  if (!checkEmpty(email)) {
    setError(emailEmptyError);
  } else if (!emailValid(email.value)) {
    setError(emailInvalidError);
  };


  // validating password checking password is empty or not and is not invalid format
  if (!checkEmpty(password)) {
    setError(passwordEmptyError);
  } else if (!passwordValid(password.value)) {
    setError(passwordInvalidError);
  }
  
  // validating password checking password is empty or not and is not invalid format
  if (!checkEmpty(confirm_password)) {
    setError(confirmPasswordEmptyError);
  } else if (!passwordMatch(password.value, confirm_password.value)) {
    setError(passwordNotMatchingError);
  }

  if (!isFormValid) {
    e.preventDefault();
  }
});

// function for checking the input value empty or not
const checkEmpty = (input) => input.value !== '';

// function for checking the email  valid with regex
const emailValid = (inputValue) => {
  const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
  return re.test(inputValue);
}

// function for checking the password valid with regex
const passwordValid = (inputValue) => {
  const re = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;
  return re.test(inputValue);
}

// function for checking the password is matching with the confirm_password
const passwordMatch = (password, confirm_password) => password === confirm_password;

// function for setting error - with classlist
const setError = (inputError) => {
  inputError.classList.remove('hidden');
  inputError.classList.add('display');
  isFormValid = false;
}

const clearValidation = () => {
  isFormValid = true;

  nameEmptyError.classList.remove('display');
  nameEmptyError.classList.add('hidden');
  
  emailEmptyError.classList.remove('display');
  emailEmptyError.classList.add('hidden');

  emailInvalidError.classList.remove('display');
  emailInvalidError.classList.add('hidden');

  emailExistError.classList.remove('display');
  emailExistError.classList.add('hidden');

  
  passwordEmptyError.classList.remove('display');
  passwordEmptyError.classList.add('hidden');
    
  passwordInvalidError.classList.remove('display');
  passwordInvalidError.classList.add('hidden');

  confirmPasswordEmptyError.classList.remove('display');
  confirmPasswordEmptyError.classList.add('hidden');

  passwordNotMatchingError.classList.remove('display');
  passwordNotMatchingError.classList.add('hidden');

}