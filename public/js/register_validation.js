const validation = (name, email, password, confirm_password, nameEmptyError, emailEmptyError, emailInvalidError, passwordEmptyError, passwordInvalidError, confirmPasswordEmptyError, passwordNotMatchingError, setError) => {
  // function for checking the input value empty or not
  const checkEmpty = (inputValue) => inputValue !== ''

  // function for checking the email  valid with regex
  const emailValid = (inputValue) => {
    const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
    return re.test(inputValue)
  }

  // function for checking the password valid with regex
  const passwordValid = (inputValue) => {
    const re = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;
    return re.test(inputValue)
  }

  // function for checking the password is matching with the confirm_password
  const passwordMatch = (password, confirm_password) =>password === confirm_password;

  // validating name - checking name is empty or not
  if (!checkEmpty(name)) {
    setError(nameEmptyError);
  }

  // validating email - checking email is empty or not and not invalid format
  if (!checkEmpty(email)) {
    setError(emailEmptyError);
  } else if (!emailValid(email)) {
    setError(emailInvalidError);
  }

  // validating password checking password is empty or not and is not invalid format
  if (!checkEmpty(password)) {
    setError(passwordEmptyError);
  } else if (!passwordValid(password)) {
    setError(passwordInvalidError);
  }

  // validating  confirm password checking confirm_password is empty or not and is matching with the password
  if (!checkEmpty(confirm_password)) {
    setError(confirmPasswordEmptyError);
  } else if (!passwordMatch(password, confirm_password)) {
    setError(passwordNotMatchingError);
  }
};

module.exports = validation ;
