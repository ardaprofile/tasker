const express = require('express');
const jwt = require('jsonwebtoken');
const { getLogger } = require('nodemailer/lib/shared');
const router = express.Router();
const db = require('../db/database');


// route for email confirmation
router.get('/confirm-email/:token', async (req, res) => {


  let token = req.params.token;
  let { authtoken } = req.session;
  let decoded = jwt.decode(token, { complete: true });
  console.log(decoded);
  let email = decoded.payload._email;
  let result = await db.one('SELECT * FROM users WHERE email = $1', [email.toLowerCase()]);
  res.render(`pages/thankyou`, {
    title: 'Confirm-Email Success | Tasker',
    topic: 'Confirm-Email Success',
    msg: 'Thank you for confirming your email. Please login',
    authtoken
  })
  await db.none('UPDATE users SET is_verified = TRUE WHERE email = $1', [email.toLowerCase()])
});

module.exports = router;