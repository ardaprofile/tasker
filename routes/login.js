const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const db = require('../db/database');
const querystring = require('querystring');
const jwt = require('jsonwebtoken');
const validation = require('../public/js/login_validation');
const middleware = require('../middlewares');

// router for getting the login page
router.get('/',middleware.logInCheck, (req, res) => {
  let { email, emailEmptyError, emailInvalidError, emailNotExistError, emailNotConfirmedError, passwordEmptyError, passwordInvalidError, passwordIncorrectError } = req.query;
  let { authtoken   } = req.session;
  res.render('pages/login', {
    title: 'Login | Tasker',
    authtoken,
    email,
    emailEmptyError,
    emailInvalidError,
    emailNotExistError,
    emailNotConfirmedError,
    passwordEmptyError,
    passwordInvalidError,
    passwordIncorrectError,
  })
});

// router for posting the login page
router.post('/', async (req, res) => {
  try {
    let { email, password } = req.body;
    let queryParams = {
      email: '',
      emailEmptyError: false,
      emailInvalidError: false,
      emailNotExistError: false,
      emailNotConfirmedError: false,
      passwordEmptyError: false,
      passwordInvalidError: false,
      passwordIncorrectError: false
    }
    let isFormValid = true;
    const setError = (inputError) => {
      queryParams[inputError] = true;
      isFormValid = false
    }
    validation(email, password, 'emailEmptyError', 'emailInvalidError', 'passwordEmptyError', 'passwordInvalidError',setError);
    let user = await db.oneOrNone('SELECT * from users where email=$1', [email.toLowerCase()]);
    if (isFormValid) {
      if (user === null) {
        setError('emailNotExistError');
        queryParams.email = email;
        const query = querystring.stringify(queryParams);
        res.redirect(`login?${query}`);
      }
    }
    if (!isFormValid) {
      
      queryParams.email = email;
      const query = querystring.stringify(queryParams);
      res.redirect(`login?${query}`);
    }
    else {
      let result = bcrypt.compareSync(password, user.password);
      if (!result) {
        setError('passwordIncorrectError');
        queryParams.email = email
        const query = querystring.stringify(queryParams)
        res.redirect(`login?${query}`)
      }
      else if (!user.is_verified) {
        setError('emailNotConfirmedError');
        queryParams.email = email;
         const query = querystring.stringify(queryParams)
         res.redirect(`login?${query}`)
      }
      else {
        // if the login is successful, create a session
        const maxAge = 1000 * 24 * 60 * 60;
        const createToken = (email) => {
          return jwt.sign({ email }, 'secretkey', {
            expiresIn: maxAge,
          })
        }
        const token = createToken(email);
        req.session.authtoken = token; 
        res.redirect('/');
      }
    } 
}
 catch(error) {
 console.log(error);
 }
})
 
 
 
module.exports = router;