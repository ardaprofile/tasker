const express = require('express')
const router = express.Router();

 // router for logout route
router.get('/', (req, res) => {
  try {
  // deleting the session and redirecting to the login page.
  req.session.destroy();
  res.redirect('/login');
 }
 catch (error) {
  console.log(error);
 }
})

module.exports = router;
