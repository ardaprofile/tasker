const express = require('express');
const router = express.Router();
let jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../db/database');
const nodemailer = require('nodemailer');
const ejs = require('ejs');
const path = require('path');
const querystring = require('querystring');
const resetformvalidation = require('../public/js/password_reset');
const validation = require('../public/js/password_forgot');
const middleware = require('../middlewares')


const SALT_ROUNDS = 10;
let MailSetup;
const port = process.env.PORT;

 // router  for getitng the forgot password page
router.get('/forgot-password', middleware.logInCheck, (req, res) => {
  let { email, modal, emailEmptyError, emailInvalidError, emailNotExistError, emailNotConfirmedError } = req.query;
  let { authtoken } = req.session;
  res.render('pages/forgot-password', {
    title: 'Forgot-Password | Tasker',
    authtoken,
    email,
    modal,
    emailEmptyError,
    emailInvalidError,
    emailNotExistError,
    emailNotConfirmedError,
  })
})

// router for posting the forgot password page
router.post('/forgot-password', async (req, res) => {
  //console.log("hello");
  try {
    const { email } = req.body;
    let queryParams = {
      email: '',
      emailEmptyError: false,
      emailInvalidError: false,
      emailMissingError: false,
      emailUnconfirmedError: false,
    }
    let isFormValid = true;
    const setError = (inputError) => {
      queryParams[inputError] = true;
      isFormValid = false;
    }
    validation(email, 'emailEmptyError', 'emailInvalidError', setError);
    //console.log(email);
    let user = await db.oneOrNone('SELECT * from users where email=$1', [email.toLowerCase()]);
    if (isFormValid) {
      if (user === null) {
        setError('emailNotExistError');
      }
      else if (!user.is_verified) {
        setError('emailNotConfirmedError');
      }
    }
    if (!isFormValid) {
      queryParams.email = email;
      const query = querystring.stringify(queryParams);
      res.redirect(`forgot-password?${query}`)
    }
    else {
      
      let email = user.email;
      let token = jwt.sign({ _email: email }, process.env.TOKEN_SECRET)
      //  console.log(token);
      //  res.status(201).send(`User added with ID:${results.id}`);
       let url_link
       if (process.env.SSL) {
         url_link = `https://tasker-app1.herokuapp.com`
       } else {
         url_link = `http://localhost:${port}`
       }
      let testAccount = await nodemailer.createTestAccount()
      let transporter = nodemailer.createTransport({
        service: process.env.MAIL_SERVICE,
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: process.env.SECURE,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASS,
        },
      })
      let template = `/app/templates/passwordreset.ejs`
      ejs.renderFile(
        template,
        { email: email, token: token, url_link: url_link },
        function (err, data) {
          if (err) {
            console.log(err);
          }
          else {
            MailSetup = {
              from: process.env.MAIL_USER,
              to: `${email}`,
              subject: 'Reset Password - Tasker',
              text: 'Reset Password - Tasker',
              html: data,
            }
          }
          
        }
      )
      const query = querystring.stringify({ modal: 'open', email: user.email });
      res.redirect(`forgot-password?${query}`);
      //  console.log(MailSetup)
      let info = await transporter.sendMail(MailSetup);
     // console.log('Message sent: %s', info.messageId)
     // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))    
    }
  } catch (error) {
    console.log(error)
  }
});

// router for getting the password reset-page
router.get('/reset-link/:token',middleware.logInCheck, (req, res) => {
  let { token } = req.params;
  let { authtoken } = req.session
  let {
    modal,
    passwordEmptyError,
    passwordInvalidError,
    confirmPasswordEmptyError,
    passwordNotMatchingError,
  } = req.query;
  res.render('pages/password-reset', {
    title: 'Reset-Password | Tasker',
    authtoken,
    token,
    modal,
    passwordEmptyError,
    passwordInvalidError,
    confirmPasswordEmptyError,
    passwordNotMatchingError,
  })
});

// router for posting the password reset-page
router.post('/reset-link/:token', async (req, res) => {
  try {
    let token = req.params.token;
    let decoded = jwt.decode(token, { complete: true });
    let email = decoded.payload._email;
    let { password, confirm_password } = req.body
    let queryParams = {
      passwordEmptyError: false,
      passwordInvalidError: false,
      confirmPasswordEmptyError: false,
      passwordNotMatchingError: false,
    }
    let isFormValid = true
    const setError = (inputError) => {
      queryParams[inputError] = true
      isFormValid = false
    }

    let user = await db.one(`SELECT * FROM users WHERE email=$1`, [email])
    if (user) {
      resetformvalidation(
        password,
        confirm_password,
        'passwordEmptyError',
        'passwordInvalidError',
        'confirmPasswordEmptyError',
        'passwordNotMatchingError',
        setError
      )
    }
    if (!isFormValid) {
      const query = querystring.stringify(queryParams)
      res.redirect(`/password/reset-link/${req.params.token}?${query}`)
    } else {
      let hashpass = bcrypt.hashSync(password, SALT_ROUNDS);
      db.none(`UPDATE users SET password=$1 WHERE email=$2`, [hashpass, email])
      const query = querystring.stringify({ modal: 'open' })
      res.redirect(`/password/reset-link/${req.params.token}?${query}`);
    }
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
