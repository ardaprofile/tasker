const express = require('express')
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../db/database');
const nodemailer = require("nodemailer");
const ejs = require('ejs');
const path = require('path');
const querystring = require('querystring');
const validation = require('../public/js/register_validation');
const middleware = require('../middlewares');
require('dotenv').config()


let MailSetup;
const SALT_ROUNDS = 10;
const port = process.env.PORT || 8005;

// route for getting the getting the register page
router.get('/',middleware.logInCheck, (req, res) => {
  let {
    name, email,nameEmptyError,emailEmptyError,emailInvalidError,emailExistError,passwordEmptyError,passwordInvalidError,confirmPasswordEmptyError,passwordNotMatchingError,
  } = req.query;
  let { authtoken } = req.session;
  res.render('pages/register', {
    title: 'Register | Tasker',
    authtoken,
    name,
    email,
    nameEmptyError,
    emailEmptyError,
    emailInvalidError,
    emailExistError,
    passwordEmptyError,
    passwordInvalidError,
    confirmPasswordEmptyError,
    passwordNotMatchingError,
  });
});



// route for getting the posting the register page
router.post('/', async (req, res) => {
  
  try {
    let { name, email, password, confirm_password } = req.body;
    let queryParams = {
      name: '',
      email: '',
      nameEmptyError: false,
      emailEmptyError: false,
      emailInvalidError: false,
      emailExistError: false,
      passwordEmptyError: false,
      passwordInvalidError: false,
      confirmPasswordEmptyError: false,
      passwordNotMatchingError: false,
    }

    let isFormValid = true;
    const setError = (inputError) => {
      queryParams[inputError] = true;

      isFormValid = false;
      
    }
    
   let user = await db.oneOrNone('SELECT * from users where email=$1', [email.toLowerCase()])
    if (user) {
      setError('emailExistError');
    } else {
      validation(name, email, password, confirm_password, 'nameEmptyError', 'emailEmptyError', 'emailInvalidError', 'passwordEmptyError', 'passwordInvalidError', 'confirmPasswordEmptyError', 'passwordNotMatchingError', setError);
    };

    if (!isFormValid) {
      // If invalid form, redirect to form with alerts and values (except passwords) that were entered before submit
      queryParams.name = req.body.name;
      queryParams.email = req.body.email;
      const query = querystring.stringify(queryParams);
      res.redirect(`/register?${query}`);
    }
    else {
      let token = jwt.sign({ _email: email }, process.env.TOKEN_SECRET);
      let authtoken = '';
      const hashpass = bcrypt.hashSync(password, SALT_ROUNDS);
      let results = await db.one('INSERT INTO users (name, email,password) VALUES ($1, $2, $3) RETURNING *', [name, email, hashpass]);
      res.render(`pages/thankyou`, { title: "Registration Success | Tasker", topic: 'Registration Success', msg: 'Thank you for registering with us. Your registration is successful, Please confirm your email address and login', authtoken });
      let url_link;
      if (process.env.SSL) {
         url_link = `https://tasker-app1.herokuapp.com`
      } else {
        url_link = `http://localhost:${port}`;
      }
      let testAccount = await nodemailer.createTestAccount();
      let transporter = nodemailer.createTransport({
        service: process.env.MAIL_SERVICE,
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: process.env.SECURE,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASS,
        },
      })
      let template = `/app/templates/mail.ejs`;
      
      ejs.renderFile(template,
        { email: req.body.email, token: token, id: results.id,url_link: url_link},
        function (err, data) {
          if (err) {
            console.log(err);
          }
          else {
             MailSetup = {
               from: process.env.MAIL_USER,
               to: `${email}`,
               subject: 'Confirm Email - Tasker',
               text: 'Confirm Email - Tasker',
               html: data,
             }
          }
         
        }
      )
      //console.log(MailSetup)
      let info = await transporter.sendMail(MailSetup)
      // console.log('Message sent: %s', info.messageId)
      //console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))
    }
  }
   catch (error) {
    console.log(error);
  }    
});




module.exports = router;


//print mailsetup, 