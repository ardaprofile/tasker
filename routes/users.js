const express = require('express');
const router = express.Router();
const db = require('../db/database.js');
const moment = require('moment');
const jwt = require('jsonwebtoken');



// router for getting the dashboard
router.get('/',  async (req, res) => {
  try {
    let { authtoken } = req.session;

    let tasks = await db.any(`SELECT id,taskname,assignedto,status,description,duedate FROM tasks ORDER BY duedate`);
    res.render('pages/taskdashboard', {
      authtoken,
      tasks: tasks,
      title: 'Task-Dashboard | Tasker',
    })
  } catch (error) {
    console.log(error);
  }
});


// router for getting the add-task
router.get('/add-task', (req, res) => {
  try {
    let { authtoken } = req.session
    res.render('pages/add-task', { title: 'Add-task | Tasker',authtoken })
  } catch (error) {
    console.log(error);
  }
});


// router for posting the add-task
router.post('/add-task', async (req, res) => {
    try {
      let { authtoken } = req.session;
      let decoded = jwt.decode(authtoken, { complete: true });
      let email = decoded.payload.email;
      let result = await db.one(`SELECT id FROM users WHERE email=$1`, [email]);
     addedTask = {
       taskname: req.body.taskName,
       assignedTo: req.body.assignedTo,
       due_date: req.body.due_date,
       status: req.body.status,
       description: req.body.description,
       user_id: result.id
      }
      
    addedTask.taskname = addedTask.taskname.charAt(0).toUpperCase() + addedTask.taskname.slice(1);
    await db.none(
      'INSERT INTO tasks (taskname, duedate,assignedto,status,description,user_id) VALUES (${addedTask.taskname}, ${addedTask.due_date},${addedTask.assignedTo},${addedTask.status},${addedTask.description},${addedTask.user_id})',{ addedTask });
      res.redirect('/');
  } catch (error) {
    console.log(error);
  } 
});

// router for getting single-task
router.get('/task/edit/:id', async (req, res) => {
  try {
    let taskId = req.params.id;
    let { authtoken } = req.session;
    const task = await db.one(`SELECT id,taskname,duedate,assignedto,status,description FROM tasks where id=$1`,[taskId])
    res.render('pages/edit-task', {
      task: task,
      title: 'Edit-task | Tasker',
      moment: moment,
      authtoken
    })
  } catch (error) {
    console.log(error);
  }
  
});

// router for editing single-task
router.put('/task/edit/:id', async (req, res) => {
  try {
    let searchQuery = { _id: req.params.id }
    //console.log(req.params.id);
    await db.none(
      `UPDATE tasks set taskname=$1, duedate=$2, status=$3,assignedto=$4,description=$5 WHERE id= $6`,
      [
        req.body.taskName,
        req.body.date_picker,
        req.body.status,
        req.body.assignedTo,
        req.body.description,
        searchQuery._id,
      ]
    )

    res.redirect('/');
    
  } catch (error) {
    console.log(error);
  }
});

// router for deleting task
router.delete('/task/delete/:id',async (req, res) => {
  try {
    let taskId = req.params.id 
    await db.none('DELETE from tasks where id=$1', [taskId])
    res.redirect('/');
  } catch (error) {
    res.redirect('/');
    console.log(error);
  }
}); 

// router for searching - based on taskname and status
router.get('/search', async (req, res) => {
  try {
    let searchQuery = { name: req.query.search_query };
    let { authtoken } = req.session; 
    searchQuery1 = searchQuery.name.charAt(0).toUpperCase() + searchQuery.name.slice(1);
    console.log()
   // console.log(searchQuery.name);
    let tasks = await db.any('SELECT id,taskname,assignedto,status,description,duedate FROM tasks WHERE taskname=$1 or status=$1',[searchQuery1]);
    res.render('pages/search', {
      tasks: tasks,
      title: 'Task-Search | Tasker',
      authtoken
    }) 
  }
  catch(error) {
    console.log(error);
  }
});

module.exports = router;

